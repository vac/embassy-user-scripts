# user-scripts

*PUBLIC* project to provide user scripts.

## How to run the get-kubeconfig script

```bash
bash <(curl -sSL https://gitlab.ebi.ac.uk/vac/embassy-user-scripts/raw/master/get-kubeconfig )
``` 

## How to run the get-password script

```bash
bash <(curl -sSL https://gitlab.ebi.ac.uk/vac/embassy-user-scripts/raw/master/get-password )
``` 
